---
title: unity
date: 2019-11-16
tags: ["游戏开发"]
categories: ["游戏开发"]
---

# 项目结构

- Assets
  - Plugins(默认目录)：存放非c#写的插件
  - Resources(默认目录)
    - 打包时，所有文件打成一个类似于AssetBundle的资源包。
    - 程序启动时，unity自动加载这个资源包的全部内容，不可跳过。如果Resources目录下资源太多，会导致游戏启动卡顿。
    - 只有Resources目录下的资源，才能在Inspector中使用。
  - StreamingAssets(默认目录)
    - 所有此目录下的文件，都不做任何处理，直接拷贝到demo.app/Data/StreamingAssets目录
    - 通过Application.streamingAssetsPath访问
  - Gizmos(默认目录)：可以在里面放一些小图，用于绘制gizmos时显示。
  - Editor(默认目录)：Editor目录可以放在工程的任何位置，unity都可以识别。
  - "Editor Default Resources"(默认目录)：存放编辑器模式使用的资源，使用EditorGUIUtility.Load读取。
  - Standard Assets(默认目录)：存放导入的第三方资源包
- Library：
  - unity不是直接使用原始的资源，而是对原始资源进行内部导入才使用的。
  - 对unity中的资源做修改后，unity会自动重新导入这些资源，导入的结果存放在Library目录
  - 不需要上传到git。

# 脚本编译顺序

1. Standard Assets,Pro Standard Assets,Plugins目录下的脚本。生成Assembly.CSharp.Plugins.csproj
2. 上面三个目录下的Editor目录下的脚本。生成Assembly.CSharp.Editor.Plugins.csproj
3. 除去上面三个目录下的脚本，一般为Scripts目录下的脚本。生成Assembly.CSharp.csproj
4. 除去上面三个目录下的Editor目录下的脚本。生Assembly.CSharp.Editor.csproj

# 文件路径

- IOS:
  - Application.dataPath : Application/random-xxx/demo.app/Data
  - Application.streamingAssetsPath : Application/random-xxx/demo.app/Data/Raw
  - Application.persistentDataPath : Application/random-xxx/Documents
  - Application.temporaryCachePath : Application/random-xxx/Library/Caches
- android:
  - Application.dataPath : /data/app/xxx.xxx.xxx.apk
  - Application.streamingAssetsPath : jar:file:///data/app/xxx.xxx.xxx.apk/!/assets
  - Application.persistentDataPath : /data/data/xxx.xxx.xxx/files
  - Application.temporaryCachePath : /data/data/xxx.xxx.xxx/cache

# 资源管理

## 资源加载方式

1. Resources目录下的资源

```c#
//同步加载方式
//respath: 资源相对于Resources的路径
Resources.Load(string respath);

//异步加载方式
Resources.LoadAsync(string respath);

//卸载某个资源
//需要注意的是，无论这个资源从哪里来，都可以用此方法卸载
Resources.UnloadAsset(object gameobject);
```

2. AssetBundle

- 将一组相互关联的资源，打包成一个AssetBundle，统一加载。
- 打包后可以放在本地，一般放在StreamingAsset目录下。
- 也可以放在服务器，用做热更。下载后放在?目录。
- <https://github.com/Unity-Technologies/AssetBundles-Browser>这个工具可以用来打包和查看AssetBundle。
  - 打包的时候，会将所有依赖都打进包里，所以打包完成后可以删除源文件。

```c#
AssetBundle ab = AssetBundle.LoadFromFile(string path);
//respath: 资源相对于原始Resources目录的路径
Object gameobject = ab.LoadAsset(string respath);
//卸载本ab中所有的资源
ab.Unload(false);

AssetBundle.LoadFromFileAsync();

//从本地或网络下载资源
WWW.LoadFromCacheOrDownload();

//unity的新接口，用于代替WWW
UnityWebRequest;
```

3. 文本资源(txt,json,xml)
   1. 可以直接使用c#原生的文件操作方式读写。

4. 自定义资源(.asset)
   1. 

5. AssetDataBase
   1. 这个接口只能在编辑器模式下使用